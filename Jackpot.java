public class Jackpot{
	public static void main (String[] args){
	System.out.println("Welcome to Jackpot!");
	Board board = new Board();
	boolean gameOver = false;
	int numOfTilesClosed =0;
	while(gameOver==false){
		System.out.println(board);
		if(board.playATurn() == true){
			gameOver=true;
		}
		else{
			numOfTilesClosed++;
		}
		
	}
	if(numOfTilesClosed>=7){
		System.out.println("You have reached the jackpot! YOU WIN!");
	}
	else{
		System.out.println("You didn't reach the jackpot... You lost, Better luck next time.");
	}
	}
}